from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from urllib.parse import urlparse
import MySQLdb as sql
from urllib.parse import urlencode
import lxml.html
import threading
import queue
import time
import random
import sys
import glob
import re
import os
import psutil
import traceback
import io
import numpy as np


class Browser:
    def __enter__(self):
        # Create an options object for the chrome driver
        options = webdriver.ChromeOptions()
        # Specify headless mode
        options.add_argument('headless')
        # Get the index of a random user-agent
        self.user_agent_idx = random.randrange(len(user_agents))
        # Specify for chrome to use the user-agent
        options.add_argument(f'user-agent={user_agents[self.user_agent_idx]}')
        # Add proxy
        options.add_argument('--proxy-server=%s' % proxy_queue.get())
        # Add language setting
        options.add_argument('--lang=en-GB,en-US;q=0.9,en;q=0.8')
        # Don't load images
        options.add_experimental_option('prefs', {'profile.managed_default_content_settings.images': 2})
        # create a new Chrome session using the chrome driver
        self.driver = webdriver.Chrome(os.path.realpath('chromedriver'), options=options)
        # Set the timeout
        self.driver.set_page_load_timeout(10)
        # Record the process id
        self.pid = self.driver.service.process.pid
        return self

    def __call__(self):
        return self.driver

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.close()
        self.driver.quit()
        # If the process is still running then force it to close
        try:
            psutil.Process(self.pid).kill()
        except:
            pass


class logger():
    def __init__(self):
        # Mark the start time of the script
        self.start = time.time()
        # Create a threading lock
        self.lock = threading.Lock()

    def log(self, string):
        """Prints with a nicely formatted string in the form [hh:mm:ss]"""
        time_passed = time.time() - self.start
        m, s = divmod(time_passed, 60)
        h, m = divmod(m, 60)
        time_string = '[{:02}:{:02}:{:02}]'.format(int(h), int(m), int(s))
        with self.lock:
            with io.open('log.txt', 'a', encoding='utf8') as f:
                f.write( '%s%s\n' % ( time_string, str(string) ) )


def extract_results(source, search_query):
    results = []
    root = lxml.html.fromstring(source)
    result_elements = root.xpath('//div[contains(concat(" ", @class, " "), "bkWMgd") and (descendant::h2[contains(translate(text(), "WEBRESULT", "webresult"),"web result")] or not(descendant::h2))]//div[contains(concat(" ", @class, " "), " g ") and descendant::a[descendant::h3]]')
    for i, result in enumerate(result_elements):
        try:
            anchor = result.xpath('.//a[descendant::h3]')[0]
            title = anchor.xpath('./h3')[0].text_content()
            url = anchor.get('href')
        except Exception:
            title = None
            url = None
            logging.log('%s' % traceback.format_exc())
            # Save the html to a file for inspection
            with io.open('html/%s.html' % search_query, 'w', encoding='utf8') as f:
                f.write(source)

        try:
            description = result.xpath('.//*[@class="st"]')[0].text_content()
        except Exception:
            description = None

        # Append search result data [rank, title, description, url]
        results.append([i + 1, title, description, url])
    return results


def proxy_producer_task():
    # Read proxies from file
    with open('proxies.txt') as f:
        proxies = np.array([p.rstrip() for p in f.readlines()])

    # Set the time that each proxy will be next availbile (initially now)
    avail = np.ones((len(proxies),), dtype=int) * time.time()
    while True:
        # Get availible proxies
        mask = avail < time.time()
        # Add availible proxies to the proxy queue
        for p in proxies[mask]:
            proxy_queue.put(p)
        
        # Set the next availible time for those proxies to be in 80 to 121 seconds in the future
        avail[mask] = avail[mask] + np.random.randint(low=80, high=121, size=len(avail[mask]))
        time.sleep(np.min(avail) - time.time())


def producer_task():
    while True:
        # Try to submit the results to the MySQL database
        try:
            # Connect to the mysql database
            db = sql.connect(host='localhost', user='glass', passwd=mysql_password, db='scrape', charset='utf8mb4')
            # Get 1000 company names from the database
            db.query('SELECT c.id, c.name FROM companies AS c WHERE c.scraped = "1970-01-01 00:00:01" LIMIT 1000')
            # Get an array of the company names and primary keys from the database
            companies = db.store_result().fetch_row(maxrows=0)
            # If there were no companies returned then exit this thread
            if len(companies) == 0:
                break
            # Iterate over the sql results and add them to the queue
            for i, name in companies:
                search_queue.put((i, name))
            # Close the connection to the database
        except Exception:
            logging.log('%s' % traceback.format_exc())
            db.rollback()
        finally:
            # Close the connection to the database
            db.close()
        time.sleep(10)
    # Add an indication for each consumer that the search queue will no longer be filled
    for i in range(n):
        search_queue.put(None)


def consumer_task():
    while True:
        try:
            start = time.time()
            with Browser() as browser:
                item = search_queue.get()
                if item is None:
                    break
                # Grab the next query in the search_query queue
                company_id, search_query = item
                # Remove double quotation marks from the company name
                search_query = search_query.replace('"', '')
                # Try to search Google
                try:
                    browser().get('https://www.google.com/search?%s' % urlencode({'q': search_query}))
                    source = browser().page_source
                except Exception:
                    logging.log('%s' % traceback.format_exc())
                    continue

                # Extract the Google results from source
                results = extract_results(source, search_query)

                if len(results) > 10 or len(results) < 6:
                    # Save the html to a file for inspection
                    with open('html/%s.html' % search_query, 'w') as f:
                        f.write(source)

                # Add the company id to each of the results
                results = [[company_id] + r for r in results]
                # Add each result to the database queue
                logging.log('[{:.2f}s][{:02}][{:02}] {}'.format(time.time() - start, browser.user_agent_idx, len(results), search_query))
                
                for result in results:
                    database_queue.put(result)
        except Exception:
            # Re-search
            search_queue.put((company_id, search_query))
    # Indicate that the database queue will no longer be filled by this thread
    database_queue.put(None)


def reporter_task():
    workers = n

    while True:
        results = []
        company_ids = []
        for i in range(1000):
            item = database_queue.get()
            if item is None:
                workers -= 1
                # If there will be no more results in the queue then break out of the for loop
                if workers == 0:
                    break
            else:
                results.append(item)
                company_ids.append([item[0]])

        # Try to submit the results to the MySQL database
        try:
            # Create a database connection for this thread
            db = sql.connect(host='localhost', user='glass', passwd=mysql_password, db='scrape', charset='utf8mb4')
            # Remove previous results
            db.cursor().executemany("""DELETE FROM results WHERE company_id=%s""", company_ids)
            # Insert results
            db.cursor().executemany("""INSERT INTO results (company_id, rank, title, description, url) VALUES (%s, %s, %s, %s, %s)""", results)
            # Update the scraped time for the company name searched
            db.cursor().executemany("""UPDATE companies SET scraped = NOW() WHERE id=%s""", company_ids)
            # Commit to the database
            db.commit()
        except Exception:
            logging.log('%s' % traceback.format_exc())
            db.rollback()
        finally:
            # Close the connection to the database
            db.close()

        # If there will be no more results in the queue then allow the thread to terminate
        if workers == 0:
            break


logging = logger()
# Read the user agents from file
with open('user_agents.txt') as f:
    user_agents = [u.rstrip() for u in f.readlines()]
# Read the mysql password from file
with open('mysql_password.txt', 'r') as f:
    mysql_password = f.read()

search_queue = queue.Queue()
database_queue = queue.Queue()
proxy_queue = queue.Queue(maxsize=1000)

producer = threading.Thread(target=proxy_producer_task, daemon=True)
producer.start()


# The number of worker threads
n = 30
# Start the producer queue to fill the serach query queue
producer = threading.Thread(target=producer_task, daemon=True)
producer.start()

# Start each of the consumer threads to do the Google searches
consumers = [threading.Thread(target=consumer_task) for i in range(n)]
for c in consumers:
    c.start()

# Start the reporter thread
reporter = threading.Thread(target=reporter_task, daemon=True)
reporter.start()
# Wait for the reporter thread to finish
# which will in turn wait for the consumer threads to finsih
# which in turn wait for the producer thread to finish
reporter.join()